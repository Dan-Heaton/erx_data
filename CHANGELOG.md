## 1.11.2 - 31/03/21
- Moved __version__.py into 'erx_data/'erx_data' subdirectory

## 1.11.1 - 30/03/21
- Changed 'datalake/batch_processing.py' to work with Fargate functionality within the Lambda

## 1.11.0 - 24/03/21
- Updated the 'Current Features' section of 'README.md'
- Added 'Setup' and 'Use Cases' sections to 'README.md'
- Modified 'find_subject_metadata()' and 'find_segmentfile_metadata()' to return lists of dictionaries 
  and dictionaries, respectively, rather than DataFrames

## 1.10.0 - 23/03/21
- Added 'create_feature_subset()' to 'batch_processing.py'

## 1.9.0 - 23/03/21
- Added 'get_task_logs()' to 'batch_processing.py'

## 1.8.0 - 22/03/21
- Added 'batch_processing.py' to 'datalake/' containing 
  'run_process_features()' and 'get_task_status()'

## 1.7.4 - 15/03/21
- Fix for optional 'metadata_df' argument to 'find_mimic_file_paths()'

## 1.7.3 - 15/03/21
- Added optional 'metadata_df' argument to 'find_mimic_file_paths()'

## 1.7.2 - 15/03/21
- Fix for import issue

## 1.7.1 - 15/03/21
- Added 'get_signals_data()' to 'erx_data/datalake/mimic.py'
- Updated the source distribution

## 1.7.0 - 15/03/21
- Added 'query_metadata()' to 'erx_data/datalake/mimic.py'
- Modified 'find_mimic_file_paths()' to return a nested dictionary instead of a list of dictionaries, with 
  keys as subject names and values as dictionaries of waveform record names to segment names
- Separated the creation of the query string in 'query_metadata()' into its own 
  'create_query_str()' function
- Added 'find_subject_metadata()' and 'find_segmentfile_metadata()' 
  to 'erx_data/datalake/mimic.py'

## 1.6.2 - 12/03/21
- Further refactoring of the 'access_data_metadata.py' script into new directory 
  and as new name ('mimic.py') with functions condensed into single function
- Modified 'find_mimic_file_paths()' to use segment names in the table rather 
  than having to download and search waveform records via 'wfdb'

## 1.6.1 - 11/03/21
- Refactored directory structure
- Added 'access_data_metadata.py'
- Modified 'get_subject_data_metadata()' to also return segment names

## 1.5.5 - 10/03/21
- Modified VitalDB metadata to include typecasting from numpy to 
  non-numpy types, along with several other explicit castings

## 1.5.4 - 10/03/21
- Fix for 'AttributeError: 'NoneType' object has no attribute 'split''
- Fix for 'TypeError: expected string or bytes-like object'

## 1.5.3 - 09/03/21
- Modified 'get_subject_data_metadata()' to no longer get segment names (as not 
  needed by batch processing during '--only_metadata' mode)

## 1.5.2 - 09/03/21
- Added 'get_subject_data_metadata()' to get metadata about the signals data files 
  themselves (and not metadata about the subject), to be used when batch processing 
  is run with '--only_metadata' optional argument

## 1.5.1 - 09/03/21
- Minor improvements to handle subject's with sparse metadata (e.g. 'p027801'), 
  including ensuring all fields are included in output (even if with None as val) 
  and attempting to get all non-hadm fields wherever possible

## 1.5.0 - 09/03/21
- Added more metadata fields including 'dob', 'hadm', 'diagnosis_hadm', 
  'diagnosis_overall', 'procedures_hadm', and 'procedures_overall'
- Modified 'prescriptions_hadm' and 'prescriptions_overall' to be list of 
  dictionaries containing prescription name, start date, and end date
- Refactored 'get_subject_metadata()' to process having 'hadm' or not in 
  the same way
- Modified several fields from 'chartevents' to be all lowercase w/ 
  no spaces, and only float or None values
- Changed 'has_htn_hadm' and 'has_htn_overall' to pick up 'HYPERTENSIVE' along 
  with 'HYPERTENSION' and 'HTN' as being hypertension conditions

## 1.4.6 - 05/03/21
- Added error handling around the getting of .hea and .dat files 
for MIMIC signals files

## 1.4.5 - 04/03/21
- Added 'get_subject_data_segments()' to 'vitaldb/subject/subject_data.py' 
  to yield file chunks rather than the complete file

## 1.4.4 - 04/03/21
- Fixed issue resulting from wfdb trying to download .hea files that exist 
according to waveform record but don't actually exists in directory
- Adds in values to the metadata dictionary that we were unable to be determined 
  due to the measure data not matching to a specific hospital admission ID

## 1.4.3 - 03/03/21
- Fixed bug with '_load_segment_signals()' in trying to get len() of an int

## 1.4.2 - 03/03/21
- Added the file num as dictionary output of '_load_segment_signals()'

## 1.4.1 - 03/03/21
- Changed '_load_segment_signals()' in 'mimic/subject/subject_data.py' to yield 
  parts of segment files rather than complete file, and modified other functions 
  to account for this change

## 1.4.0 - 03/03/21
- Added 'subject_data.py' and 'subject_metadata.py' scripts to
  'vitaldb' directory

## 1.3.1 - 01/03/21
- Added handling of failure to load files via '_load_segment_signals()'

## 1.3.0 - 01/03/21
- Added function 'get_subject_data_segments()' to mimic/subject/subject_data.py
- Fixed bug in subject age metadata due to the shifting of DoBs for subjects 
  over 89 by MIMIC by setting them to an age of 90

## 1.2.1 - 25/02/21
- Replaced the 'MANIFEST.in' with __init__.py files (should now properly 
  function as a source distribution)
- Fix to typo in 'setup.py' causing version number to appear as '-1.2.1-' 
  instead of '1.2.1'

## 1.2.0 - 25/02/21
- Added 'MANIFEST.in'
- Added 'dist' subdirectory
- Moved source code to inner 'erx_data' subdirectory

## 1.1.0 - 25/02/21
- Added 'setup.py'
- Fixed typo in 'requirements.txt'

## 1.0.0 - 24/02/21
- Initial commit, including basic data lake access/creation/query functionality, 
  along with MIMIC subject data and metadata retrieval