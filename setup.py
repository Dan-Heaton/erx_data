
from setuptools import setup, find_packages
from codecs import open

with open("README.md", encoding="utf-8") as f:
    long_description = f.read()

with open("erx_data/__version__.py") as f:
    __version__ = f.read().split()[-1].strip("\"")

setup(
    name="erx_data",
    version=__version__,

    description="ElectronRx's Data Access Library",
    long_description=long_description,

    url="https://bitbucket.org/electronrx/erx_data",

    author="Dan Heaton",
    author_email="dheaon@electronrx.com",
    license="MIT",
    keywords="erx data access",
    packages=find_packages(),
)