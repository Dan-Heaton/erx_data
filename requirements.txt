boto3==1.17.5
botocore==1.20.5
python-dateutil==2.8.1
numpy==1.20.1
pandas==1.2.2
wfdb==3.2.0