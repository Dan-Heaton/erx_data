# erx_data - ElectronRx's Data Access Library

Ongoing project to map all ElectronRx's data access requirements to our own, 
bespoke modules and functions.
  - Helps to make data access easier and abstract much of the data engineering 
    aspects away from users of the data
  - Standardises the way we work with data, and allows for more fine-grained 
    security management of different parts of our data
  - Should help speed up project development in minimising time spent accessing 
    and retrieving data
  - Project agnostic, and users free to utilise data for variety of projects 
    (given the requisite permission to access the data)
    
### Current Features
  - Creation of datasets as tables within the data lake from subsets of data, 
    querying of datasets, and retrieval of datasets
  - Retrieval of MIMIC subject's data and metadata from MIMIC-III Waveform Database
    Matched Subset and MIMIC-III Clinical Database, respectively
  - Retrieval of VitalDB subject's data and metadata from vitaldb.net
  - Query and filter tables of metadata in the datalake via Athena wrapper 
    to find matching subjects given a filter, find S3 file paths corresponding 
    to this filter, and find specific subject's metadata in the tables
  - Run batch processing jobs to create datasets of features with specified feature 
    sets (e.g. 'gaurav') and subsets of subjects (determined via inclusion/exclusion criteria)
    
## Setup
To setup 'erx_data', clone it via:

```console
 pip install git+https://<USER NAME>@bitbucket.org/Dan-Heaton/erx_data.git
```

To update the version, update via:

```console
 pip install git+https://<USER NAME>@bitbucket.org/Dan-Heaton/erx_data.git --upgrade
```

Note that this may be updated soon to be available via PyPI, which would make it 
available via ```pip install erx-data```. Watch this space!


## Use Cases
The following is aimed at covering the various use cases of 'erx_data' as it stands. It should serve 
as an easy guide to map from a data science-based engineering requirement to what to use in 'erx_data'. 
As before, this is an ongoing guide which will be added to as more functionality is added to the library.


#### Use Case
"I want to find the metadata for a specific subject from MIMIC"

#### Solution
```console
import pprint
from erx_data.datalake.mimic import find_subject_metadata

metadata = find_subject_metadata("p000107")
pprint.pprint(metadata)
```
Output (truncated):
```console
[{'admission_weight_kg': '88.6',
  'admit_date': None,
  'age': None,
  'diagnosis_hadm': None,
  'diagnosis_overall': ['AV FISTULA;ANGIOPLASTY',
                        'HYPERKALEMIA;CONGESTIVE HEART FAILURE',
                        'UPPER GI BLEED'],
  'discharge_date': None,
  'dob': '2052-04-02',
  'dod': None,
  'gender': 'M',
  'hadm': None,
  'has_htn_hadm': None,
  'has_htn_overall': 'false',
  'height': '66.0',
  'height_cm': '168.0',
  'prescriptions_hadm': [],
  'prescriptions_overall': [{'enddate': '2122-05-18',
                             'prescription': 'Metoprolol Tartrate',
                             'startdate': '2122-05-16'},
                            {'enddate': '2115-02-21',
                             'prescription': 'Metoprolol XL',
  ...
```


#### Use Case
"I want to find the metadata of all subjects in MIMIC or VitalDB that match given certain metadata values 
(or ranges of values) (e.g. male subjects age between 45 and 60, and diagnosed w/ hypertension)"

#### Solution
```console
import pandas as pd
from erx_data.datalake.mimic import create_query_str, query_metadata

query_str = create_query_str(age_lower=45, age_upper=60, gender="M", has_htn_overall=True)
df = query_metadata(query_str)
pd.set_option("display.max_columns", None)
pd.options.display.width = 0
print(df)
```
Output (truncated):
```console
   subject_name gender         dob         dod age height height_cm admission_weight_kg    hadm  admit_date discharge_date                                     diagnosis_hadm has_htn_hadm                                  diagnosis_overall has_htn_overall                            services                                 prescriptions_hadm                              prescriptions_overall                                    procedures_hadm                                 procedures_overall    wfr_date                  wfr_name                                      segment_names
0       p075525      M  2071-10-17        None  52   72.0     183.0                98.0  163773  2124-05-27     2124-05-30                             HYPERTENSIVE EMERGENCY         true  [HYPERTENSIVE EMERGENCY, HYPERTENSIVE EMERGENC...            true                [Neurologic Medical]  [{prescription=Amlodipine, startdate=2124-05-2...  [{prescription=Amlodipine, startdate=2124-10-0...  [{category=Access Lines - Peripheral, label=18...  [{category=Access Lines - Peripheral, label=20...  2124-05-27  p075525-2124-05-27-23-59  [3696046_0001, 3696046_0002, 3696046_0003, 369...
1       p075525      M  2071-10-17        None  52   72.0     183.0                98.0  163773  2124-05-27     2124-05-30                             HYPERTENSIVE EMERGENCY         true  [HYPERTENSIVE EMERGENCY, HYPERTENSIVE EMERGENC...            true                [Neurologic Medical]  [{prescription=Amlodipine, startdate=2124-05-2...  [{prescription=Amlodipine, startdate=2124-10-0...  [{category=Access Lines - Peripheral, label=18...  [{category=Access Lines - Peripheral, label=20...  2124-05-27  p075525-2124-05-27-21-10                       [3526792_0001, 3526792_0002]
2       p026212      M  2133-11-10  2190-06-30  56   63.0     160.0                54.0  159674  2190-01-14     2190-01-22                                           S/P FALL        false  [S/P FALL, HYPERTENSIVE EMERGENCY, HYPERTENSIO...            true                           [Medical]  [{prescription=Amlodipine, startdate=2190-01-1...  [{prescription=Amlodipine, startdate=2190-01-1...  [{category=Access Lines - Peripheral, label=18...  [{category=Access Lines - Peripheral, label=20...  2190-01-18  p026212-2190-01-18-12-58  [3738230_0001, 3738230_0002, 3738230_0003, 373...
3       p010595      M  2139-02-05        None  51   None      None                None  107278  2190-05-27     2190-06-04                                       HYPERTENSION         true          [DISECTING AORTIC ANEURYSM, HYPERTENSION]            true                   [Cardiac Medical]  [{prescription=Amlodipine Besylate, startdate=...  [{prescription=Amlodipine Besylate, startdate=...                                                 []                                                 []  2190-05-31  p010595-2190-05-31-14-34  [3099108_0001, 3099108_0002, 3099108_0003, 309...
4       p010595      M  2139-02-05        None  51   None      None                None  107278  2190-05-27     2190-06-04                                       HYPERTENSION         true          [DISECTING AORTIC ANEURYSM, HYPERTENSION]            true                   [Cardiac Medical]  [{prescription=Amlodipine Besylate, startdate=...  [{prescription=Amlodipine Besylate, startdate=...                                                 []                                                 []  2190-06-01  p010595-2190-06-01-16-01                                     [3997755_0001]
5       p010595      M  2139-02-05        None  51   None      None                None  107278  2190-05-27     2190-06-04                                       HYPERTENSION         true          [DISECTING AORTIC ANEURYSM, HYPERTENSION]            true                   [Cardiac Medical]  [{prescription=Amlodipine Besylate, startdate=...  [{prescription=Amlodipine Besylate, startdate=...                                                 []                                                 []  2190-05-27  p010595-2190-05-27-13-22  [3906530_0001, 3906530_0002, 3906530_0003, 390...
6       p024320      M  2096-06-08        None  57   None      None                None  100916  2153-06-27     2153-06-29  PRESYNCOPE;PALPITATIONS;CORONARY ARTERY DISEAS...         true  [PRESYNCOPE;PALPITATIONS;CORONARY ARTERY DISEA...            true                   [Cardiac Medical]  [{prescription=Metoprolol, startdate=2153-06-2...  [{prescription=Metoprolol, startdate=2153-06-2...                                                 []                                                 []  2153-06-27  p024320-2153-06-27-17-12                                     [3079831_0001]
7       p041050      M  2065-08-22        None  58   68.0     173.0               105.5  149007  2123-08-25     2123-08-27                             HYPERTENSIVE EMERGENCY         true                           [HYPERTENSIVE EMERGENCY]            true                [Neurologic Medical]  [{prescription=Metoprolol Tartrate, startdate=...  [{prescription=Metoprolol Tartrate, startdate=...  [{category=Access Lines - Peripheral, label=20...  [{category=Access Lines - Peripheral, label=20...  2123-08-25  p041050-2123-08-25-11-39  [3628621_0001, 3628621_0002, 3628621_0003, 362...
8       p041050      M  2065-08-22        None  58   68.0     173.0               105.5  149007  2123-08-25     2123-08-27                             HYPERTENSIVE EMERGENCY         true                           [HYPERTENSIVE EMERGENCY]            true                [Neurologic Medical]  [{prescription=Metoprolol Tartrate, startdate=...  [{prescription=Metoprolol Tartrate, startdate=...  [{category=Access Lines - Peripheral, label=20...  [{category=Access Lines - Peripheral, label=20...  2123-08-25  p041050-2123-08-25-05-17  [3383360_0001, 3383360_0002, 3383360_0003, 338...
9       p083418      M  2084-01-21        None  48   72.0     183.0                80.0  118975  2132-02-25     2132-02-28                     HYPERTENSIVE URGENCY;TELEMETRY         true                   [HYPERTENSIVE URGENCY;TELEMETRY]            true          [Cardiac Medical, Medical]  [{prescription=Metoprolol Tartrate, startdate=...  [{prescription=Metoprolol Tartrate, startdate=...  [{category=Access Lines - Peripheral, label=18...  [{category=Access Lines - Peripheral, label=18...  2132-02-25  p083418-2132-02-25-23-59  [3596380_0001, 3596380_0002, 3596380_0003, 359...
...
```


#### Use Case
"I want to map all results for a given metadata filter (i.e. specific metadata values or ranges of values) to the 
locations of their corresponding signals data files in S3"

#### Solution
```console
import pprint
from erx_data.datalake.mimic import create_query_str, find_mimic_file_paths

query_str = create_query_str(age_lower=25, age_upper=27)
fps = find_mimic_file_paths(query_str)
pprint.pprint(fps)
```
Output (truncated):
```console
{'p000109': {'p000109-2142-08-13-05-59': []},
 'p002578': {'p002578-2125-05-05-19-41': []},
 'p003764': {'p003764-2140-02-27-10-41': [], 'p003764-2140-03-14-09-04': []},
 'p005163': {'p005163-2111-09-28-04-16': []},
 'p006233': {'p006233-2105-07-24-18-53': [], 'p006233-2105-07-25-01-09': []},
 'p008415': {'p008415-2148-03-09-02-24': []},
 'p008557': {'p008557-2187-08-13-04-25': []},
 'p010604': {'p010604-2193-12-31-13-07': [], 'p010604-2194-01-06-15-39': []},
 'p011861': {'p011861-2132-07-28-22-05': [],
             'p011861-2132-08-05-00-37': [],
             'p011861-2133-02-02-17-06': []},
 'p013099': {'p013099-2103-10-18-05-53': [], 'p013099-2103-10-18-22-40': []},
 'p013355': {'p013355-2159-05-08-16-30': [],
             'p013355-2159-05-09-09-58': [],
             'p013355-2159-05-10-10-18': []},
 'p014714': {'p014714-2151-01-12-19-51': ['mimic/p014714/3293176_0003/0.csv.gz',
                                          'mimic/p014714/3293176_0004/0.csv.gz',
                                          'mimic/p014714/3293176_0006/0.csv.gz',
                                          'mimic/p014714/3293176_0007/0.csv.gz',
                                          'mimic/p014714/3293176_0008/0.csv.gz',
                                          'mimic/p014714/3293176_0008/1.csv.gz',
                                          'mimic/p014714/3293176_0008/2.csv.gz',
                                          'mimic/p014714/3293176_0008/3.csv.gz',
                                          'mimic/p014714/3293176_0008/4.csv.gz',
                                          'mimic/p014714/3293176_0008/5.csv.gz',
                                          'mimic/p014714/3293176_0008/6.csv.gz',
                                          'mimic/p014714/3293176_0008/7.csv.gz'],
             'p014714-2151-01-14-10-47': ['mimic/p014714/3582815_0008/0.csv.gz',
                                          'mimic/p014714/3582815_0010/0.csv.gz',
                                          'mimic/p014714/3582815_0012/0.csv.gz',
                                          'mimic/p014714/3582815_0014/0.csv.gz',
                                          'mimic/p014714/3582815_0014/1.csv.gz',
                                          'mimic/p014714/3582815_0014/2.csv.gz',
...
```


#### Use Case
"I want to create a feature dataset in S3 for all subjects matching a specific inclusion/exclusion criteria for a 
specific feature set (e.g. 'gaurav')"

#### Solution
```console
from erx_data.datalake.batch_processing import create_feature_subset

create_feature_subset("mimic", "gaurav", feature_output_dir="test_one",
                      age_lower=40, age_upper=60, gender="M", has_htn_overall=True)
```
Output (truncated):
```console
		------ 1.) Creating query ------

		------ 2.) Running query on metadata Glue table via Athena ------
Getting metadata matches from MIMIC metadata table via query 'SELECT * from signals_mimic_metadata WHERE gender='M' AND has_htn_overall=True AND age BETWEEN 40 AND 60'...
Querying table 'signals_mimic_metadata' in database 'datalake_open_source_signals_db'...
Successfully queried data from 'signals_mimic_metadata' in database 'datalake_open_source_signals_db'!...
Matched 20 subjects via the query: '['p000518', 'p001900', 'p068720', 'p005727', 'p025329', 'p010595', 'p059225', 'p026212', 'p021431', 'p077553', 'p024573', 'p024320', 'p075525', 'p066208', 'p012849', 'p083418', 'p017798', 'p041050', 'p032605', 'p053731']'

		------ 3.) Running Fargate task on 'mimic' dataset to create 'gaurav' features ------
Created 1 Fargate task(s) created with task ID's ['7bbdecc83a1148e481d3168b74b4f2d0']...

Task status: {'task_id': '7bbdecc83a1148e481d3168b74b4f2d0', 'status': 'PROVISIONING', 'start_time': '2021-03-24 15:14:59', 'end_time': 'None', 'run_time': '0 days, 0 hours, 0 minutes, 0 seconds'}

Task status: {'task_id': '7bbdecc83a1148e481d3168b74b4f2d0', 'status': 'PROVISIONING', 'start_time': '2021-03-24 15:14:59', 'end_time': 'None', 'run_time': '0 days, 0 hours, 0 minutes, 5 seconds'}

Task status: {'task_id': '7bbdecc83a1148e481d3168b74b4f2d0', 'status': 'PROVISIONING', 'start_time': '2021-03-24 15:14:59', 'end_time': 'None', 'run_time': '0 days, 0 hours, 0 minutes, 10 seconds'}

Task status: {'task_id': '7bbdecc83a1148e481d3168b74b4f2d0', 'status': 'PROVISIONING', 'start_time': '2021-03-24 15:14:59', 'end_time': 'None', 'run_time': '0 days, 0 hours, 0 minutes, 16 seconds'}

Task status: {'task_id': '7bbdecc83a1148e481d3168b74b4f2d0', 'status': 'PENDING', 'start_time': '2021-03-24 15:14:59', 'end_time': 'None', 'run_time': '0 days, 0 hours, 0 minutes, 21 seconds'}
...
...
Task status: {'task_id': '7bbdecc83a1148e481d3168b74b4f2d0', 'status': 'RUNNING', 'start_time': '2021-03-24 15:14:59', 'end_time': 'None', 'run_time': '0 days, 0 hours, 5 minutes, 38 seconds'}
------ Cloudwatch events ------

Task status: {'task_id': '7bbdecc83a1148e481d3168b74b4f2d0', 'status': 'RUNNING', 'start_time': '2021-03-24 15:14:59', 'end_time': 'None', 'run_time': '0 days, 0 hours, 5 minutes, 43 seconds'}
------ Cloudwatch events ------
1/20: Processing subject 'p000518'...
Subject 'p000518' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
2/20: Processing subject 'p001900'...
Subject 'p001900' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
3/20: Processing subject 'p068720'...
Subject 'p068720' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
4/20: Processing subject 'p005727'...
Subject 'p005727' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
5/20: Processing subject 'p025329'...
Subject 'p025329' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
6/20: Processing subject 'p010595'...
Subject 'p010595' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
7/20: Processing subject 'p059225'...
Subject 'p059225' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
8/20: Processing subject 'p026212'...
Subject 'p026212' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
9/20: Processing subject 'p021431'...
Subject 'p021431' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
10/20: Processing subject 'p077553'...
Subject 'p077553' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
11/20: Processing subject 'p024573'...
Subject 'p024573' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
12/20: Processing subject 'p024320'...
Subject 'p024320' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
13/20: Processing subject 'p075525'...
Subject 'p075525' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
14/20: Processing subject 'p066208'...
Subject 'p066208' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
15/20: Processing subject 'p012849'...
Subject 'p012849' has no segments paths in 'open-source-pulses-v2' (no pulses files due to no segment files due to not having both ABP and PPG signals)...
16/20: Processing subject 'p083418'...
16/20: 1/20: Processing segment 'mimic/p083418/3770464_0009/0.csv.gz'...
16/20: 2/20: Processing segment 'mimic/p083418/3770464_0009/1.csv.gz'...
16/20: 3/20: Processing segment 'mimic/p083418/3770464_0009/2.csv.gz'...
16/20: 4/20: Processing segment 'mimic/p083418/3770464_0010/0.csv.gz'...
16/20: 5/20: Processing segment 'mimic/p083418/3770464_0010/1.csv.gz'...
16/20: 6/20: Processing segment 'mimic/p083418/3770464_0010/10.csv.gz'...
16/20: 7/20: Processing segment 'mimic/p083418/3770464_0010/11.csv.gz'...
16/20: 8/20: Processing segment 'mimic/p083418/3770464_0010/12.csv.gz'...
16/20: 9/20: Processing segment 'mimic/p083418/3770464_0010/13.csv.gz'...
16/20: 10/20: Processing segment 'mimic/p083418/3770464_0010/14.csv.gz'...
16/20: 11/20: Processing segment 'mimic/p083418/3770464_0010/15.csv.gz'...
16/20: 12/20: Processing segment 'mimic/p083418/3770464_0010/16.csv.gz'...
16/20: 13/20: Processing segment 'mimic/p083418/3770464_0010/2.csv.gz'...
16/20: 14/20: Processing segment 'mimic/p083418/3770464_0010/3.csv.gz'...
...
...
ploaded 'mimic/p032605/3588460_0042/0.csv.gz' to 'open-source-features-v2/output_test_one_gaurav_2021-03-24'
Uploaded 'mimic/p032605/3925152_0003/5.csv.gz' to 'open-source-features-v2/output_test_one_gaurav_2021-03-24'
Uploaded 'mimic/p032605/3925152_0003/6.csv.gz' to 'open-source-features-v2/output_test_one_gaurav_2021-03-24'
Uploaded 'mimic/p032605/3925152_0003/7.csv.gz' to 'open-source-features-v2/output_test_one_gaurav_2021-03-24'
Uploaded total of 76 to 'open-source-features-v2/output_test_one_gaurav_2021-03-24'!

Task status: {'task_id': '7bbdecc83a1148e481d3168b74b4f2d0', 'status': 'DEPROVISIONING', 'start_time': '2021-03-24 15:14:59', 'end_time': 'None', 'run_time': '0 days, 0 hours, 6 minutes, 22 seconds'}

Task status: {'task_id': '7bbdecc83a1148e481d3168b74b4f2d0', 'status': 'DEPROVISIONING', 'start_time': '2021-03-24 15:14:59', 'end_time': 'None', 'run_time': '0 days, 0 hours, 6 minutes, 27 seconds'}

Task status: {'task_id': '7bbdecc83a1148e481d3168b74b4f2d0', 'status': 'STOPPED', 'start_time': '2021-03-24 15:14:59', 'end_time': '2021-03-24 15:21:31', 'run_time': '0 days, 0 hours, 6 minutes, 31 seconds'}
```
