import boto3
import time
import json
from datetime import datetime, timezone

from erx_data.datalake.mimic import create_query_str, query_metadata


def run_fargate(dataset: str, feature_set: str, subject_names: list = None, num_subjects: int = None,
                num_tasks: int = 1, feature_output_dir: str = None):
    """
        Run a Fargate task to process features from pulse files in the 'open-sources-pulses-v2' bucket

        Parameters
        ----------
        dataset : str
            The name of the dataset of which to process the pulses (e.g. 'mimic')
        feature_set : str
            The name of the feature set to use to create the feature files (e.g. 'gaurav')
        subject_names : list
            Optional list of subject names by which to filter the dataset; if not provided, either limits to a maximum
            of 'num_subjects' subjects to process (if provided) or all subjects
        num_subjects : int
            Optional maximum number of subjects to randomly select from the dataset to create the feature files;
            if not provided, either limits to specific subjects by 'subject_names' (if provided) or all subjects
        num_tasks : int
            Optional number of tasks (maximum 10) to launch with this specification; defaults to 1
        feature_output_dir : str
            Optional additional identifier for the feature directory in 'open-source-signals-v2' that will be created

        Return
        ----------
        task_ids : list
            List of task ID's of the newly-created Fargate tasks
    """

    event = locals()
    lambda_client = boto3.client("lambda")
    invoke_response = lambda_client.invoke(
        FunctionName="batch-processing-fargate-RunFargateFunction-1AZMUX4GD87PK",
        LogType='None',
        Payload=json.dumps(event))

    payload = json.loads(invoke_response["Payload"].read())
    if payload["statusCode"] == 200:
        task_ids = payload["body"]["response"]
        print(f"Created {num_tasks} Fargate task(s) created with task ID's {task_ids}...")
        return task_ids
    else:
        print(f"Unable to access Lambda to run Fargate task: {payload['body']['message']}")
        return None


def get_task_status(task_id: str):
    """
        Gets the status of a Fargate batch processing task associated with a specific task ID

        Parameters
        ----------
        task_id : str
            The task ID of the Fargate instance

        Return
        ----------
        task_dict : dict
            The status of the Fargate task, including its task ID, its status (e.g. 'RUNNING'), the time it
            was started, the time it ended (if it ended), and the total time it ran for
    """

    fargate = boto3.client("ecs")
    response = fargate.describe_tasks(cluster="batch-processing-cluster",
                                      tasks=[task_id])
    if not response["tasks"]:
        print(f"No task matching '{task_id}' task ID...")
        return None
    response_task = response["tasks"][0]

    # Gets the start and stop times (if task has stopped) and calculates the total duration of the task,
    # or the duration it has run so far
    start_time, stop_time = response_task.get("createdAt"), response_task.get("stoppedAt")
    run_timedelta = stop_time - start_time if stop_time else datetime.now(timezone.utc) - start_time
    days = run_timedelta.days
    hours, remainder = divmod(run_timedelta.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)

    task_dict = {"task_id": task_id,
                 "status": response_task["lastStatus"],
                 "start_time": str(start_time).split(".")[0],
                 "end_time": str(stop_time).split(".")[0],
                 "run_time": f"{days} days, {hours} hours, {minutes} minutes, {seconds} seconds"}

    return task_dict


def get_task_logs(task_id: str, limit: int = 10000, start_from_head: bool = False, time_interval: int = None):
    """
        Gets the Cloudwatch logs of a Fargate batch processing task associated with a specific task ID

        Parameters
        ----------
        task_id : str
            The task ID of the Fargate instance
        limit : int
            Optional limit of the maximum number of log events to return; defaults to 10,000, which is the maximum
            number of log events that can fit as determined by the response size
        start_from_head : bool
            Optional bool of whether to start fetching the earliest logs; defaults to False, which gets the
            most recent N logs, where N=limit
        time_interval : int
            Optional time interval in seconds to set the logs to return the logs between this point and now
            (e.g. setting this to 10 would return all Cloudwatch events from 10 seconds ago up until this point

        Return
        ----------
        messages : list
            The list of strings, where each message represents a single log event stored in Cloudwatch
    """

    logs = boto3.client("logs")

    # Gets the start and end times in milliseconds, of which to use to fetch the logs (note that the end time is
    # actually 'time_intervals' seconds before the current time, to account for a disparity between the time claimed
    # by a cloudwatch event and when it actually appears as a log event
    if time_interval:
        end_time = int(time.time() * 1000) - (1000 * time_interval)
        start_time = end_time - (1000 * time_interval)
    else:
        end_time, start_time = None, None

    response = logs.get_log_events(logGroupName="/ecs/batch-processing-task",
                                   logStreamName=f"ecs/batch-processing/{task_id}",
                                   startTime=start_time,
                                   endTime=end_time,
                                   limit=limit,
                                   startFromHead=start_from_head)
    messages = [event["message"] for event in response.get("events", [])]
    return messages


def create_feature_subset(dataset: str, feature_set: str, update_interval: int = 5, feature_output_dir: str = None,
                          subject_name: str = None, age: int = None, gender: str = None, has_htn_hadm: bool = None,
                          has_htn_overall: bool = None, age_lower: int = None, age_upper: int = None,
                          height_lower: float = None, height_upper: float = None, segment_name: str = None):
    """
        Creates a features dataset in S3 from the subset of all subjects who fit the various metadata parameters;
        it takes in parameters to filter the subjects (e.g. all subjects between age 25 - 30, gender of male, and
        diagnosed with HTN at some point) for a specific dataset (e.g. 'mimic'), gets the names of the subjects that
        match these parameters by querying via Athena, creates a Fargate batch processing job to process the features
        of these subjects using 'feature_set' feature processing (e.g. 'gaurav'), and monitors the task's status
        and Cloudwatch logs

        Parameters
        ----------
        dataset : str
            The name of the dataset of which to process the pulses (e.g. 'mimic')
        feature_set : str
            The name of the feature set to use to create the feature files (e.g. 'gaurav')
        update_interval : int
            The time interval in seconds to use to update the status and most recent Cloudwatch logs
            of a Fargate task
        feature_output_dir : str
            Optional additional identifier for the feature directory in 'open-source-signals-v2' that will be created
        subject_name : str
            Optional name of the subject by which to filter the table results
        age : int
            Optional age of subjects by which to filter the table results
        gender : str
            Optional gender of the subjects (either 'M' or 'F') by which to filter the table results
        has_htn_hadm : bool
            Optional bool of whether subjects have been diagnosed with HTN on the specific admission by which to filter
            the table results
        has_htn_overall : bool
            Optional bool of whether subjects have been diagnosed with HTN on any of their admissions by which to filter
            the table results
        age_lower : int
            Optional lower bound of the ages by which to filter the table results
        age_upper : int
            Optional upper bound of the ages by which to filter the table results
        height_lower : float
            Optional lower bound of the heights (in inches) by which to filter the table results
        height_upper : float
            Optional upper bound of the heights (in inches) by which to filter the table results
        segment_name : str
            Optional name of a segment file matching to a subject name and waveform record name by which to filter
            the table results
    """

    args = locals()
    for arg in ("dataset", "feature_set", "update_interval", "feature_output_dir"):
        args.pop(arg)

    print("\n\t\t------ 1.) Creating query ------")
    query_str = create_query_str(**args)

    print("\n\t\t------ 2.) Running query on metadata Glue table via Athena ------")
    df = query_metadata(query_str)
    # Gets the unique subject names that match the filtering query
    subject_names = list(set(df.loc[:, "subject_name"].tolist()))
    print(f"Matched {len(subject_names)} subjects via the query: '{subject_names}'")

    print(f"\n\t\t------ 3.) Running Fargate task on '{dataset}' dataset to create '{feature_set}' features ------")
    task_id = run_fargate(dataset=dataset, feature_set=feature_set,
                          subject_names=subject_names, feature_output_dir=feature_output_dir)[0]

    state = None
    while state != "STOPPED":
        task_dict = get_task_status(task_id)
        state = task_dict["status"]
        print(f"\nTask status: {task_dict}")
        if state == "RUNNING":
            print("------ Cloudwatch events ------")
            for message in get_task_logs(task_id, time_interval=update_interval):
                print(message)
        time.sleep(update_interval)


if __name__ == "__main__":

    create_feature_subset("mimic", "gaurav", feature_output_dir="test_one",
                          age_lower=40, age_upper=60, gender="M", has_htn_overall=True)
