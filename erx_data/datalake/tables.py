import os
import gzip
import json
import boto3
import numpy as np


def create_table(table: str, database: str, target: str, bucket: str, columns: list,
                 subdirectory: str = None, description: str = None, file_format: str = "csv"):
    """
        Creates a table in a database in the datalake from a target within a bucket by passing the
        function params to the DatalakeCreateTableFunction Lambda

        Parameters
        ----------
        table : str
            The name of the table to create
        database : str
            The name of the database that will contain the table
        target : str
            The target within the bucket in question that we wish to
            use as a table (e.g. subject name)
        bucket : str
            The name of the bucket in which to search for the table target
        columns : list
            Defines the column names within the table schema
        subdirectory : str
            Optional name of the subdirectory within the bucket to search for the table
            target directory (e.g. "new_mimic")
        description : str
            Optional description to give the table
        file_format : str
            Format of the file that will be used to make the table (defaults to .csv)
    """

    # Extracts all the arguments into a dict mapping parameters to parameter value
    event = locals()

    lambda_client = boto3.client("lambda")
    invoke_response = lambda_client.invoke(
        FunctionName="datalake-create-DatalakeCreateTableFunction-1T5VEJ5LGK0YM",
        LogType='None',
        Payload=json.dumps(event))

    payload = json.loads(invoke_response["Payload"].read())
    if payload["statusCode"] == 200:
        print(f"Successfully create table '{table}' in database '{database}'!")
    else:
        print(f"Unable to create table '{table}' in database '{database}': {payload['body']['message']}")


def query_table(table: str, database: str, filter_datasets: list = None, filter_subjects: list = None,
                max_results: int = None, delete_bucket_output: bool = False, query_str: str = None):
    """
        Takes a specific table within a database in the data catalog, queries it via AWS Athena, and returns the
        metadata and a sample of the data via the DatalakeQueryTableFunction Lambda function

        Parameters
        ----------
        table : str
            Name of the table which comprises the dataset in question
        database : str
            Name of the database in which to search for the table
        filter_datasets : list
            Optional argument to filter results from the table by dataset containing subjects (e.g. 'p000107')
        filter_subjects : list
            Optional argument to filter results fom the table by subject IDs (e.g. '3860035_0025')
        max_results : int
            Optional argument to limit the number of rows of data returned
        delete_bucket_output : bool
            Optional argument to delete the output of query stored in the output S3 bucket (so the only output
            produced is returned through this function)
        query_str : str
            Optional argument that overrides the functionality of '_construct_query()' if provided to create
            the query string

        Return
        ----------
        response : dict
            Dictionary containing the data resulting from the querying and pagination process, along with
            certain metadata components; returns empty dict if query failed
    """

    # Extracts all the arguments into a dict mapping parameters to parameter value
    event = locals()

    lambda_client = boto3.client("lambda")
    print(f"Querying table '{table}' in database '{database}'...")
    invoke_response = lambda_client.invoke(
        FunctionName="datalake-query-DatalakeQueryTableFunction-1XFWARAK8D492",
        LogType='None',
        Payload=json.dumps(event))

    payload = json.loads(invoke_response["Payload"].read())
    if payload["statusCode"] == 200:
        print(f"Successfully queried data from '{table}' in database '{database}'!...")
        return payload["body"]["response"]
    else:
        print(f"Unable to query table '{table}' in database '{database}': {payload['body']['message']}")
        return {}


def access_table(table: str, database: str):
    """
        Takes a specific table within the data lake and returns all the source file paths associated
        with it in the source S3 bucket

        Parameters
        ----------
        table : str
            The name of the table in the data lake
        database : str
            The name of the database in the data lake in which the table is contained

        Return
        ----------
        data : dict
            The data being a dictionary where keys are S3 key names of bucket files and the
            corresponding values are the data downloaded from those files
    """

    lambda_client = boto3.client("lambda")
    s3 = boto3.client("s3")

    invoke_response = lambda_client.invoke(
        FunctionName="datalake-access-DatalakeAccessTableFunction-1LBHRO3EY2QFG",
        LogType='None',
        Payload=json.dumps({"table": table, "database": database}))

    payload = json.loads(invoke_response["Payload"].read())

    data = {}
    if payload["statusCode"] == 200:
        file_paths = payload["body"]["response"]
        print(f"Retrieved data from {len(file_paths)} files for table '{table}' in database '{database}'...")
        for file_path in file_paths:
            bucket_name, key = file_path
            if key.endswith(".csv.gz"):
                fn = "tmp.csv.gz"
                s3.download_file(Bucket=bucket_name, Key=key, Filename=fn)
                lines = []
                with gzip.open(fn) as f:
                    for line in f.readlines():
                        if isinstance(line, bytes):
                            line = line.decode("utf-8")
                        line = line.split("\n")[0].split(",")
                        lines.append(line)
                data[key] = lines
                os.remove(fn)
            else:
                fn = "tmp.json"
                s3.download_file(Bucket=bucket_name, Key=key, Filename=fn)
                with open("tmp.json") as f:
                    data[key] = json.load(f)
                os.remove(fn)
    else:
        print(f"Unable to load data: {payload['body']['message']}")

    return data


if __name__ == "__main__":

    create_event = {
        "database": "test_datalake_open_source_signals_db",
        "table": "signals_mimic_p000188",
        "target": "p000188",
        "bucket": "test-datalake-open-source-signals",
        "columns": ["PPG", "ABP"],
        "subdirectory": "new_mimic"}

    query_event = {
        "database": "datalake_open_source_signals_db",
        "table": "signals_mimic_metadata"}

    access_event = {
        "database": "datalake_open_source_signals_db",
        "table": "signals_mimic_p000107"}

    # create_table(**create_event)

    r = query_table(**query_event)
    print(json.dumps(r, indent=4, sort_keys=True))

    # d = access_table(**access_event)
    # for k, v in d.items():
    #     print(f"{k} : {v}")
