import pandas as pd
import boto3
import pprint
import numpy as np
import os

from erx_data.datalake.tables import query_table


BUCKET = "open-source-signals-v2"
MIMIC_DIR = "mimic"
GLUE_DATABASE = "datalake_open_source_signals_db"
METADATA_TABLE = "signals_mimic_metadata"


def _unpack_metadata(metadata: dict):
    """
        Unpacks metadata that is returned from Athena queries into its original .json format

        Parameters
        ----------
        metadata : dict
            The metadata being one row of an Athena query result

        Return
        ----------
        metadata : dict
            The same metadata but with dictionary and list strings 'unpacked' into their
            non-string dictionary/list versions (i.e. original format of the corresponding .json)
    """

    for k, v in metadata.items():
        if v and v.startswith("["):
            if v.startswith("[{"):
                if len(v) > 4:
                    v_formatted = [{k_v_str.split("=")[0]: k_v_str.split("=")[1] for k_v_str in dict_str.split(", ")}
                                   for dict_str in v[2:-2].split("}, {")]
                else:
                    v_formatted = []
            else:
                v_formatted = v[1:-1].split(", ") if len(v) > 2 else []
            metadata[k] = v_formatted
    return metadata


def create_query_str(subject_name: str = None, age: int = None, gender: str = None, has_htn_hadm: bool = None,
                     has_htn_overall: bool = None, age_lower: int = None, age_upper: int = None,
                     height_lower: float = None, height_upper: float = None, segment_name: str = None):
    """
        Creates a valid SQL query to pass to Athena to query the 'METADATA_TABLE' table via the parameters that are
        passed to the function

        Parameters
        ----------
        subject_name : str
            Optional name of the subject by which to filter the table results
        age : int
            Optional age of subjects by which to filter the table results
        gender : str
            Optional gender of the subjects (either 'M' or 'F') by which to filter the table results
        has_htn_hadm : bool
            Optional bool of whether subjects have been diagnosed with HTN on the specific admission by which to filter
            the table results
        has_htn_overall : bool
            Optional bool of whether subjects have been diagnosed with HTN on any of their admissions by which to filter
            the table results
        age_lower : int
            Optional lower bound of the ages by which to filter the table results
        age_upper : int
            Optional upper bound of the ages by which to filter the table results
        height_lower : float
            Optional lower bound of the heights (in inches) by which to filter the table results
        height_upper : float
            Optional upper bound of the heights (in inches) by which to filter the table results
        segment_name : str
            Optional name of a segment file matching to a subject name and waveform record name by which to filter
            the table results

        Return
        ----------
        query_str : str
            The completed SQL query string that can be used to query the 'METADATA_TABLE' table via Athena
    """

    query_str = f"SELECT * from {METADATA_TABLE} "
    query_additions = []

    query_dict = {"subject_name": subject_name, "age": age, "gender": gender,
                  "has_htn_hadm": has_htn_hadm, "has_htn_overall": has_htn_overall}
    query_additions += [f"{k}='{v}'" if isinstance(v, str) else f"{k}={v}" for k, v in query_dict.items() if v]

    if age_lower or age_upper:
        if age_lower and age_upper:
            query_additions.append(f"age BETWEEN {age_lower} AND {age_upper}")
        else:
            raise ValueError("Must provide both 'age_lower' and 'age_upper' args if providing at least one...")
    if height_lower or height_lower:
        if height_lower and height_upper:
            query_additions.append(f"height BETWEEN {height_lower} AND {height_upper}")
        else:
            raise ValueError("Must provide both 'height_lower' and 'height_upper' args "
                             "if providing at least one...")
    if segment_name:
        query_additions.append(f"CONTAINS(segment_names, '{segment_name}')")

    # Joins up the additions to the query string so that the query string remains as valid SQL
    if query_additions:
        query_str += f"WHERE {query_additions[0]}"
        if len(query_additions) > 1:
            query_str += " AND " + " AND ".join(query_additions[1:])

    return query_str


def query_metadata(query_str: str):
    """
        Queries the metadata AWS Glue table 'signals_mimic_metadata' (created from the 'open-sources-signals-v2/
        signals_metadata' S3 directory) with either an explicit string ('query_str'), and returns the result of
        this query via Athena as a Pandas DataFrame

        Parameters
        ----------
        query_str : str
            Optional string to define the query string explicitly (i.e. the exact query to be passed to Athena)
    """

    print(f"Getting metadata matches from MIMIC metadata table via query '{query_str}'...")
    data = query_table(database=GLUE_DATABASE, table=METADATA_TABLE, query_str=query_str).get("data")
    df = pd.DataFrame.from_records(data[1:], columns=data[0])
    return df


def find_subject_metadata(subject_name: str):
    """
        Gets the metadata for a specific subject (e.g. "p000107") in the metadata table as a DataFrame

        Parameters
        ----------
        subject_name : str
            The name of the subject for which to find the metadata

        Return
        ----------
        metadata_records : list
            The metadata of the subject as a list of dictionaries, with each dictionary being the metadata
            of the subject for a specific date (i.e. waveform record name)
    """

    query_str = create_query_str(subject_name=subject_name)
    df = query_metadata(query_str)
    metadata_records = [_unpack_metadata(metadata) for metadata in df.to_dict("records")]
    return metadata_records


def find_segmentfile_metadata(segment_name: str, subject_name: str = None):
    """
        Gets the metadata for a specific segment name (e.g. "3860035_0025") in the metadata table as a DataFrame;
        note that we're only expecting a single metadata entry, unlike 'find_subject_metadata()' which may
        receive multiple metadata entries

        Parameters
        ----------
        segment_name : str
            The name of the segment file for which to find the metadata
        subject_name : str
            Optional name of the subject to help filter the query (e.g. in case we expect multiple subjects
            to reference the same segment name)

        Return
        ----------
        metadata : dict
            The metadata corresponding to a subject's specific segment file as a dictionary
    """

    query_str = create_query_str(subject_name=subject_name, segment_name=segment_name)
    df = query_metadata(query_str)
    metadata_record = _unpack_metadata(df.to_dict("records")[0])
    return metadata_record


def get_signals_data(segment_paths: list):
    """
        Gets the signals data (i.e. ABP and PPG signals) for the segment paths in question and returns this data
        as a nested dictionary

        Parameters
        ----------
        segment_paths : list
            A list of strings, where each is the path to a S3 signals data file in the 'BUCKET' bucket

        Return
        ----------
        data_dict : dict
            The nested dictionary with the keys being the names of the segment paths and the values being a list
            of dictionaries, where each dictionary contains the ABP and PPG signal for a single time instance
    """

    tmp_path = "tmp.csv.gz"
    s3 = boto3.client('s3')

    data_dict = {}
    for segment_path in segment_paths:
        s3.download_file(Bucket=BUCKET, Key=segment_path, Filename=tmp_path)
        data = pd.read_csv(tmp_path).loc[:, ["PPG", "ABP"]]
        data_dict[segment_path] = data.to_dict("records")
    os.remove(tmp_path)

    return data_dict


def find_mimic_file_paths(query_str: str, metadata_df: pd.DataFrame = None):
    """
        Maps a query onto the 'signals_mimic_metadata' table via Athena and returns with a dictionary of results, with
        each key being the subject name and the value being a dictionary of waveform record names for this subject and
        their respective file names in the S3 bucket; empty if no valid (i.e. with both ABP and PPG signals values)
        files for this subject name and waveform record name; this enables one to map from a metadata-based filter
        query to all the matching signals file names

        Parameters
        ----------
        query_str : str
            The SQL query string that is used to find corresponding entries in
            the metadata 'signals_mimic_metadata' table
        metadata_df : pd.DataFrame
            Optional DataFrame argument of metadata already retrieved, so as to skip the retrieval of metadata
            via the 'query_str' argument

        Return
        ----------
        file_paths_mappings : dict
            The dictionary of results from Athena, with each key being the subject name and the value being a dict of
            waveform record names for this subject and their respective file names in the S3 bucket as a list
    """

    # Queries the 'signals_mimic_metadata' table via Athena and formats the results as records and only including
    # the subject name, waveform record name, and segment names
    print(f"Getting metadata matches from MIMIC metadata table via query '{query_str}'...")
    metadata_df = query_metadata(query_str=query_str) if metadata_df is None else metadata_df
    subjects_wfr_segments = metadata_df[["subject_name", "wfr_name", "segment_names"]].to_dict("records")

    file_paths_mappings = {}
    print(f"Returned with {len(subjects_wfr_segments)} results from query; matching subject name / waveform record "
          f"name combinations to signal files...")
    paginator = boto3.client("s3").get_paginator("list_objects_v2")

    # For each record (i.e. row from the Athena query), translate the segment names column values
    # (e.g. '3981049_0002') into their locations in the S3 bucket, unless they don't appear there (i.e. due
    # to not containing both ABP and PPG signals and thus being filtered by batch processing)
    for i, subject_wfr_segments in enumerate(subjects_wfr_segments):
        subject_name, wfr_name, segment_names = \
           subject_wfr_segments["subject_name"], subject_wfr_segments["wfr_name"], subject_wfr_segments["segment_names"]

        # Gets all the signals files (ending in 'csv.gz') in S3 that correspond to a specific person
        # (e.g.within the 'S3://<BUCKET>/<MIMIC_DIR>/<subject_name>/' directory)
        pages = paginator.paginate(Bucket=BUCKET, Prefix=f"{MIMIC_DIR}/{subject_name}")
        file_paths = [content["Key"] for page in pages for content in page.get("Contents", {})
                      if content["Key"].endswith(".csv.gz")]

        # Filters all file paths (pointing to segment files in the S3 bucket) by those corresponding
        # to the waveform in question: this ensures that the file paths we return aren't just all file paths
        # for a subject but only those that match the results of the query
        print(f"{i + 1} / {len(subjects_wfr_segments)}: Mapping subject '{subject_name}' and waveform record "
              f"name '{wfr_name}' to their .csv files in S3 from segment names....")

        # Only keeps file paths in S3 that correspond to the segment names for the row in question
        file_paths_filtered = [file_path for file_path in file_paths if file_path.split("/")[2] in segment_names]

        # Populates the nested dictionary where a subject name matches to a dictionary with keys being the waveform
        # record name and the values being the segment names
        if subject_name in file_paths_mappings:
            file_paths_mappings[subject_name][wfr_name] = file_paths_filtered
        else:
            file_paths_mappings[subject_name] = {wfr_name: file_paths_filtered}

    return file_paths_mappings


if __name__ == "__main__":
    q = "select * from signals_mimic_metadata where age between 25 and 26"
    fps = find_mimic_file_paths(q)
    pprint.pprint(fps)

    subject = "p000107"
    subject_df = find_subject_metadata(subject)
    pprint.pprint(subject_df)

    seg_name = "3860035_0025"
    segment_df = find_segmentfile_metadata(segment_name=seg_name, subject_name=subject)
    print(segment_df)

    d_dict = get_signals_data(segment_paths=['mimic/p046116/3975150_0006/0.csv.gz',
                                             'mimic/p046116/3975150_0006/1.csv.gz',
                                             'mimic/p046116/3975150_0006/2.csv.gz'])
    for key, val in d_dict.items():
        print(f"{key} : {np.shape(val)}")
