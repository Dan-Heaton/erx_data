"""
    Versioning convention

    'A.X.X' - Massive overhaul of the library, new features, new ways of accessing resources, etc.
    'X.A.X' - New scripts, functions, capabilities, etc.
    'X.X.A' - Bug fixes and/or other minor tweaks
"""

__version__ = "1.11.2"
