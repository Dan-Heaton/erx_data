import re
import json
import numpy as np
import pandas as pd
import boto3
import datetime
import pprint
from dateutil.relativedelta import relativedelta


ITEMID_MAPPING = {
    "Admission Weight (Kg)": 226512,
    "Height": 226707,
    "Height (cm)": 226730
}

SERVICE_MAPPING = {
    "CMED": "Cardiac Medical",
    "CSURG": "Cardiac Surgery",
    "DENT": "Dental",
    "ENT": "Ear, nose, and throat",
    "GU": "Genitourinary",
    "GYN": "Gynecological",
    "MED": "Medical",
    "NB": "Newborn",
    "NBB": "Newborn baby",
    "NMED": "Neurologic Medical",
    "NSURG": "Neurologic Surgical",
    "OBS": "Obstetrics",
    "ORTHO": "Orthopaedic",
    "OMED": "Orthopaedic medicine",
    "PSURG": "Plastic",
    "PSYCH": "Psychiatric",
    "SURG": "Surgical",
    "TRAUM": "Trauma",
    "TSURG": "Thoracic Surgical",
    "VSURG": "Vascular Surgical"
}

# Only includes those we are interested in and as they appear in 'prescriptions' table in MIMIC-III
# (see https://docs.google.com/document/d/16FyIl2tX-9EZo3FanewuWLLRkax9qC_NwSlL50YSA_o/edit?ts=602e8c8d)
PHARMACEUTICAL_AGENTS = [
    "Amlodipine", "Amlodipine Besylate", "amlodipine-benazepril", "Diltiazem", "Diltiazem Extended-Release",
    "diltiazem", "*NF* Diltiazem SR", "Atenolol", "Atenolol-Chlorthalidone", "Bisoprolol Fumarate",
    "bisoprolol fumarate", "bisoprolol", "Carvedilol", "Metoprolol", "Metoprolol XL", "Metoprolol Tartrate",
    "Metoprolol Succinate XL", "Metoprolol XL (Toprol XL)", "Gabapentin", "Gabapentin (Liquid)", "gabapentin",
    "Clonazepam", "Sertraline", "Sertraline HCl", "Citalopram", "Citalopram Hydrobromide", "escitalopram",
    "Escitalopram Oxalate", "Fluoxetine", "Fluoxetine HCl", "Alprazolam", "Trazodone HCl", "Venlafaxine",
    "Venlafaxine XR", "desvenlafaxine"]


def _get_subject_prefix_id(subject_name: str):
    """
        Takes in a full subject name in MIMIC (e.g. 'p011957') and extracts the prefix used by the MIMIC-III
        Waveform Database Matched Subset (e.g. 'p01') and the subject ID (e.g. 11957) to reference
        in MIMIC-III Clinical Database

        Parameters
        ----------
        subject_name : str
            Name of the subject as appears in the MIMIC-III Waveform Database Matched Subset

        Return
        ----------
        subject_prefix : str
            Prefix of the subject name used to organise in the waveform database
        subject_id : id
            ID of the subject that corresponds to the subject in the clinical database
    """

    subject_prefix = subject_name[:3]
    subject_id = int(subject_name[1:].lstrip("0"))
    return subject_prefix, subject_id


def _create_query(table: str, subject_id: int, measures: list = None, hadm: str = None,
                  prescriptions: bool = False, console_output: bool = True):
    """
        Creates an SQL query based on input parameters to be used by Athena to query the MIMIC-III Clinical
        Database for subject metadata (to be passed on to the 'QueryMimicClinicalFunction' Lambda function)

        Parameters
        ----------
        table : str
            Name of the table within the clinical database in which to apply the query
        subject_id : int
            The ID of the subject in the clinical database to retrieve information about
        measures : list
            Optional list of measurements (e.g. ['Admission Weight (Kg)', 'Height']) to filter by
            when searching in the 'chartevents' table
        hadm : str
            Optional hospital admission ID by which to filter the queries
        prescriptions : bool
            Optionally set to True if wishing to filter by drugs prescribed being in
            'PHARMACEUTICAL_AGENTS' when searching in the 'prescriptions' table
        console_output : bool
            Optionally set to False if not wishing to output the created query string to console

        Return
        ----------
        query : str
            The created SQL query string from the arguments provided to be passed on to Athena
    """

    query = f"SELECT * from {table} WHERE subject_id={subject_id}"
    if measures:
        itemid_str = str([ITEMID_MAPPING[measure] for measure in measures]).replace("[", "(").replace("]", ")")
        query += f" AND itemid IN {itemid_str}"
    if hadm:
        query += f" AND hadm_id={hadm}"
    if prescriptions:
        prescriptions_str = str(PHARMACEUTICAL_AGENTS).replace("[", "(").replace("]", ")")
        query += f" AND drug IN {prescriptions_str}"

    if console_output:
        print(f"Created query '{query}'...")

    return query


def _query_mimic_clinical(query: str, console_output: bool = True):
    """
        Queries the MIMIC-3 Clinical Database via the associated Lambda function, which uses
        the Athena service provided with the query and extracts the response as a DataFrame

        Parameters
        ----------
        query : str
            The created SQL query string from the arguments provided to be passed on to Athena
        console_output : bool
            Optionally set to False if not wishing to output when query was successful

        Return
        ----------
        df : pd.DataFrame
            The results of the query made by Athena to the clinical database as a DataFrame;
            returns None if query was unsuccessful
    """

    event = {"query": query}

    lambda_client = boto3.client("lambda")
    invoke_response = lambda_client.invoke(
        FunctionName="query-mimic-QueryMimicClinicalFunction-1EA0KAVZQDM2O",
        LogType='None',
        Payload=json.dumps(event))

    payload = json.loads(invoke_response["Payload"].read())
    if payload["statusCode"] == 200:
        if console_output:
            print(f"Successfully queried the MIMIC-3 Clinical Database'!")
        extracted_rows = payload["body"]["response"]
        df = pd.DataFrame(extracted_rows[1:], columns=extracted_rows[0])
        return df
    else:
        print(f"Unable to query the MIMIC-3 Clinical Database: {payload['body']['message']}")
        return None


def get_subject_patients(subject_name: str, console_output: bool = True):
    """
        Gets subject information from the 'patients' table in the MIMIC-III Clinical Database
        that includes their gender, date of birth, and date of death

        Parameters
        ----------
        subject_name : str
            Name of the subject as appears in the MIMIC-III Waveform Database Matched Subset
        console_output : bool
            Optionally set to False if not wishing to output the created query string to console
            or when the query was successful

        Return
        ----------
        data : dict
            The information from the 'patients' table on 'subject_name' containing their
            gender, their date of birth, and date of death (if relevant)
    """

    _, subject_id = _get_subject_prefix_id(subject_name)
    query = _create_query(table="patients", subject_id=subject_id, console_output=console_output)
    df = _query_mimic_clinical(query, console_output)
    gender, dob, dod = df.loc[0, ["gender", "dob", "dod"]].to_list()
    dob_obj = datetime.datetime.strptime(dob.split(" ")[0], "%Y-%m-%d")
    dod_obj = datetime.datetime.strptime(dod.split(" ")[0], "%Y-%m-%d") if dod else None
    data = {"gender": gender, "dob": dob_obj, "dod": dod_obj}
    return data


def get_subject_admissions(subject_name: str, measure_date: str, console_output: bool = True):
    """
        Gets subject information from the 'admissions' table in the MIMIC-III Clinical Database
        for a given date (e.g. date ABP/PPG signals were extracted) that includes their hospital
        admission ID, date of admission, date of discharge, whether they've been diagnosed with
        HTN on this visit, whether they've been diagnosed with HTN on any visits at all, their
        diagnosis for this admission, and all diagnoses across all admissions

        Parameters
        ----------
        subject_name : str
            Name of the subject as appears in the MIMIC-III Waveform Database Matched Subset
        measure_date : str
            The date as a 'YYYY-MM-DD' string of which we wish to get the corresponding
            subject's admission data
        console_output : bool
            Optionally set to False if not wishing to output the created query string to console
            or when the query was successful

        Return
        ----------
        data : dict
            The information from the 'admissions' table on 'subject_name' containing their
            hospital admission ID, admission date, discharge date, and boolean values
            corresponding to whether they had HTN on either this admission or during any
    """

    _, subject_id = _get_subject_prefix_id(subject_name)
    query = _create_query(table="admissions", subject_id=subject_id, console_output=console_output)
    df = _query_mimic_clinical(query, console_output)
    data = {}
    for _, row in df.iterrows():
        measure_date_obj = datetime.datetime.strptime(measure_date, "%Y-%m-%d")
        admit_date_obj = datetime.datetime.strptime(row["admittime"].split(" ")[0], "%Y-%m-%d")
        disch_date_obj = datetime.datetime.strptime(row["dischtime"].split(" ")[0], "%Y-%m-%d")
        if admit_date_obj <= measure_date_obj <= disch_date_obj:
            hadm = row["hadm_id"]
            data = {"hadm": hadm, "admit_date": admit_date_obj.date(),
                    "discharge_date": disch_date_obj.date()}
            break

    # If there was not a matching hospital admission for this date, 'data' will still be an empty dict
    if not data:
        data = {k: None for k in ("hadm", "admit_date", "discharge_date")}

    # Only fetches the '_hadm' metadata fields if we have a 'hadm' in the first place
    if data["hadm"]:
        # By definition, unique rows for given 'hadm_id' in 'admissions', so below should only ever return a row
        admission_diagnosis = df[df["hadm_id"] == data["hadm"]]["diagnosis"].to_list()[0]
        data["diagnosis_hadm"] = admission_diagnosis
        # Gets both the whether the subject is experiencing HTN during this current visit
        if re.match(".*HYPERTENS.*", admission_diagnosis) or re.match(".*HTN.*", admission_diagnosis):
            data["has_htn_hadm"] = True
        else:
            data["has_htn_hadm"] = False
    else:
        data["diagnosis_hadm"] = None
        data["has_htn_hadm"] = None

    all_diagnoses = df["diagnosis"].to_list()
    data["diagnosis_overall"] = all_diagnoses

    has_had_htn = False
    for diagnosis in all_diagnoses:
        # Handles if the diagnosis is empty for that row
        if diagnosis:
            if re.match(".*HYPERTENS.*", diagnosis) or re.match(".*HTN.*", diagnosis):
                has_had_htn = True
                break
    data["has_htn_overall"] = has_had_htn

    return data


def get_subject_prescriptions(subject_name: str, hadm: str = None, console_output: bool = True):
    """
        Gets subject information from the 'prescriptions' table in the MIMIC-III Clinical Database
        that includes the prescriptions prescribed during the current admission and all prescriptions
        prescribed over all the subject's admissions

        Parameters
        ----------
        subject_name : str
            Name of the subject as appears in the MIMIC-III Waveform Database Matched Subset
        hadm : str
            Optional str of the hospital admission ID (if not provided, only returns all prescriptions
            prescribed to the subject)
        console_output : bool
            Optionally set to False if not wishing to output the created query string to console
            or when the query was successful

        Return
        ----------
        data : dict
            The information from the 'prescriptions' table on 'subject_name', with each value being a list
            of prescription dictionaries, and each prescription dictionary containing the prescription name,
            start date, and end date
    """

    _, subject_id = _get_subject_prefix_id(subject_name)
    query = _create_query(table="prescriptions", subject_id=subject_id, prescriptions=True,
                          console_output=console_output)
    df = _query_mimic_clinical(query, console_output)

    data = {}
    if hadm:
        rows = df[df["hadm_id"] == str(hadm)]
        data["prescriptions_hadm"] = [{"prescription": row["drug"],
                                       "startdate": row["startdate"].split(" ")[0] if row["startdate"] else None,
                                       "enddate": row["enddate"].split(" ")[0] if row["enddate"] else None}
                                      for _, row in rows.iterrows()]
    else:
        data["prescriptions_hadm"] = []
    data["prescriptions_overall"] = [{"prescription": row["drug"],
                                      "startdate": row["startdate"].split(" ")[0] if row["startdate"] else None,
                                      "enddate": row["enddate"].split(" ")[0] if row["enddate"] else None}
                                     for _, row in df.iterrows()]
    return data


def get_subject_services(subject_name: str, hadm: str = None, console_output: bool = True):
    """
        Gets subject information from the 'services' table in the MIMIC-III Clinical Database
        that includes the services that were involved (e.g. surgery, dental, etc.) for the
        subject for that specific hospital admission

        Parameters
        ----------
        subject_name : str
            Name of the subject as appears in the MIMIC-III Waveform Database Matched Subset
        hadm : str
            Optional str of the hospital admission ID (if not provided, returns all services
            that the subject received over all admissions)
        console_output : bool
            Optionally set to False if not wishing to output the created query string to console
            or when the query was successful

        Return
        ----------
        data : dict
            The information from the 'services' table on 'subject_name' containing the
            services they received
    """

    _, subject_id = _get_subject_prefix_id(subject_name)
    query = _create_query(table="services", subject_id=subject_id, hadm=hadm, console_output=console_output)
    df = _query_mimic_clinical(query, console_output)

    # Chooses only the 'curr_service' column values, removes duplicates, sorts them alphabetically,
    # and maps them to their full names
    services = df.loc[:, "curr_service"].to_list()
    services = sorted(list(set(services)))
    data = {"services": [SERVICE_MAPPING[service] for service in services]}

    return data


def get_subject_chartevents(subject_name: str, hadm: str = None, console_output: bool = True):
    """
        Gets subject information from the 'chartevents' table in the MIMIC-III Clinical Database
        that includes their height and weight recorded on a specific hospital admission (if not
        provided, averages each over all admissions)

        Parameters
        ----------
        subject_name : str
            Name of the subject as appears in the MIMIC-III Waveform Database Matched Subset
        hadm : str
            Optional str of the hospital admission ID (if not provided, averages measurements
            over all admissions)
        console_output : bool
            Optionally set to False if not wishing to output the created query string to console
            or when the query was successful

        Return
        ----------
        data : dict
            The information from the 'chartevents' table on 'subject_name' containing their
            height in inches, height in CM, and admission weight in KG
    """

    _, subject_id = _get_subject_prefix_id(subject_name)

    measurements = ["Height", "Height (cm)", "Admission Weight (Kg)"]
    data = {measure: None for measure in measurements}

    # First, attempts to infer the subject info based on a specific 'hadm' (hospital admission ID) provided
    query = _create_query(table="chartevents", subject_id=subject_id, measures=measurements,
                          hadm=hadm, console_output=console_output)
    df = _query_mimic_clinical(query, console_output)
    for measure in measurements:
        # If provided the 'hadm' and if we have results returned, then presuming we will only have 1 entry
        # for a given measurement on a specific visit and so can safely just take the first entry
        df_subset = df.loc[df["itemid"] == str(ITEMID_MAPPING[measure])]
        if not df_subset.empty:
            # Using iloc w/ index 0 breaks here due to being of a subset, hence the workaround below
            data[measure] = df_subset.loc[:, "value"].tolist()[0]

    # If we weren't able to fill in the 'height_weight_gender' dictionary with measurements
    # from a specific 'hadm', average the readings over all the subject's visits
    if any(val is None for val in data.values()):
        measurements_no_hadm = [measure for measure, val in data.items() if not val]
        query = _create_query(table="chartevents", subject_id=subject_id, measures=measurements_no_hadm,
                              console_output=console_output)
        df = _query_mimic_clinical(query, console_output)
        for measure in measurements_no_hadm:
            df_subset = df.loc[df["itemid"] == str(ITEMID_MAPPING[measure])]
            if not df_subset.empty:
                vals = [float(val) for val in df_subset.loc[:, "value"].tolist()]
                data[measure] = np.mean(vals)

    # Ensures each non-None value into the data dictionary is as a float value
    data = {measure: float(val) if val else None for measure, val in data.items()}
    # Removes any parenthesis, spaces, and puts to lowercase for each measurement name
    # (i.e. 'Admission Weight (Kg)' becomes 'admission_weight_kg')
    data = {measure.replace("(", "").replace(")", "").replace(" ", "_").lower(): val for measure, val in data.items()}

    return data


def get_subject_procedures(subject_name: str, hadm: str = None, console_output: bool = True):
    """
        Gets subject information from the 'procedures' table in the MIMIC-III Clinical Database
        that includes details of all procedures done for a given hospital admission and for
        all hopsital admissions, with the values being a list of procedure dictionaries, and
        each dictionary containing the procedure category, label, value, start time, and end time

        Parameters
        ----------
        subject_name : str
            Name of the subject as appears in the MIMIC-III Waveform Database Matched Subset
        hadm : str
            Optional str of the hospital admission ID (if not provided, only gets 'procedures_overall')
        console_output : bool
            Optionally set to False if not wishing to output the created query string to console
            or when the query was successful

        Return
        ----------
        data : dict
            The information from the 'procedures' table on 'subject_name', with each value being a list
            of procedure dictionaries, and each prescription dictionary containing the procedure category,
            label, value, start time, and end time
    """

    _, subject_id = _get_subject_prefix_id(subject_name)
    query = _create_query(table="procedureevents_mv", subject_id=subject_id, console_output=console_output)
    df = _query_mimic_clinical(query, console_output)
    data = {}

    # Gets a list of dictionaries of all procedures carried out on the subject for a specific hospital
    # admission if 'hadm' is provided
    procedures_hadm = []
    if hadm:
        df_hadm = df[df["hadm_id"] == str(hadm)]
        # Gets the DataFrame mapping the item IDs to their labels and categories
        item_ids = df_hadm.loc[:, "itemid"].tolist()
        if item_ids:
            itemid_str = str([int(item_id) for item_id in item_ids]).replace("[", "(").replace("]", ")")
            # Can't use '_create_query()' here, as that assumes one is passing in a subject ID as well
            df_items = _query_mimic_clinical(f"SELECT * FROM d_items WHERE itemid IN {itemid_str}", console_output)

            # Adds a dictionary for each row in the procedures, with mappings to 'd_items' table to get the label
            # and category from the item_id
            for _, row in df_hadm.iterrows():
                itemid_to_label = df_items[df_items["itemid"] == row["itemid"]].iloc[0, :]
                procedures_hadm.append(
                    {"category": itemid_to_label["category"], "label": itemid_to_label["label"], "value": row["value"],
                     "starttime": row["starttime"], "endtime": row["endtime"]})
    data["procedures_hadm"] = procedures_hadm

    # Gets a list of dictionaries of all procedures carried out on the subject for all hospital admissions
    procedures_overall = []
    # Gets the DataFrame mapping the item IDs to their labels and categories
    item_ids = df.loc[:, "itemid"].tolist()
    if item_ids:
        itemid_str = str([int(item_id) for item_id in item_ids]).replace("[", "(").replace("]", ")")
        # Can't use '_create_query()' here, as that assumes one is passing in a subject ID as well
        df_items = _query_mimic_clinical(f"SELECT * FROM d_items WHERE itemid IN {itemid_str}", console_output)

        # Adds a dictionary for each row in the procedures, with mappings to 'd_items' table to get the label
        # and category from the item_id
        for _, row in df.iterrows():
            itemid_to_label = df_items[df_items["itemid"] == row["itemid"]].iloc[0, :]
            procedures_overall.append(
                {"category": itemid_to_label["category"], "label": itemid_to_label["label"], "value": row["value"],
                 "starttime": row["starttime"], "endtime": row["endtime"]})
    data["procedures_overall"] = procedures_overall

    return data


def get_subject_metadata(subject_name: str, measure_date: str = None, console_output: bool = True):
    """
        Gets subject metadata from all relevant tables in the MIMIC-III Clinical Database using each
        of the above functions and combines them into a single dictionary

        Parameters
        ----------
        subject_name : str
            Name of the subject as appears in the MIMIC-III Waveform Database Matched Subset
        measure_date : str
            The date as a 'YYYY-MM-DD' string of which we wish to get the corresponding
            subject's admission data; if not provided, metadata is limited to only certain
            non-admission-specific categories
        console_output : bool
            Optionally set to False if not wishing to output the created query string to console
            or when the query was successful

        Return
        ----------
        data : dict
            The information from the all relevant tables in the clinical database on 'subject_name'
            for a given measure data (if provided)
    """

    metadata = {}
    data_patients = get_subject_patients(subject_name, console_output)
    metadata["gender"] = data_patients["gender"]
    metadata["dob"] = str(data_patients["dob"]).split(" ")[0] if data_patients["dob"] else None
    metadata["dod"] = str(data_patients["dod"]).split(" ")[0] if data_patients["dod"] else None

    # Note: without a 'measure_date', we are unable to infer many of the metadata fields at all (as many are related
    # to a specific visit in question), and also has to infer weight/height as averaged over all visits
    if measure_date:
        data_admissions = get_subject_admissions(subject_name, measure_date, console_output)
        hadm = data_admissions.get("hadm")
        if not hadm:
            print(f"For subject '{subject_name}' on measure date '{measure_date}', unable to find hospital admission "
                  f"within the 'admissions' table; unable to add admission-specific metadata...")

        data_prescriptions = get_subject_prescriptions(subject_name, hadm, console_output)
        data_services = get_subject_services(subject_name, hadm, console_output)
        data_chartevents = get_subject_chartevents(subject_name, hadm, console_output)
        data_procedures = get_subject_procedures(subject_name, hadm, console_output)

        metadata["age"] = relativedelta(data_admissions["admit_date"], data_patients["dob"]).years if hadm else None
        # Handles the shifting of DoBs for subjects over 89 by MIMIC by setting them to an age of 90
        if metadata["age"] and metadata["age"] > 89:
            metadata["age"] = 90

        # Combines all the existing metadata into single dictionary
        metadata = {**metadata, **data_chartevents, **data_admissions, **data_services,
                    **data_prescriptions, **data_procedures}
        # Converts datetime objects into string objects at this point (after having been used to determine 'age' field)
        # to avoid JSON-serialization problems further on
        if metadata["admit_date"]:
            metadata["admit_date"] = str(metadata["admit_date"])
        if metadata["discharge_date"]:
            metadata["discharge_date"] = str(metadata["discharge_date"])

    else:
        height_weight = get_subject_chartevents(subject_name, None, console_output)
        metadata = {**metadata, **height_weight}

    return metadata


if __name__ == '__main__':
    # meta = get_subject_metadata(subject_name="p000109", measure_date="2141-10-21")
    # meta = get_subject_metadata(subject_name="p049650", measure_date="2137-11-13")
    # meta = get_subject_metadata(subject_name="p049022", measure_date="2160-12-03")
    meta = get_subject_metadata(subject_name="p027801", measure_date="2130-12-06")
    pprint.pprint(meta)
