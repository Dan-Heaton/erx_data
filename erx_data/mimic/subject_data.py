import os
import re
import sys
import json
import wfdb
import requests
import time
import shutil
import traceback
import numpy as np


TEMP_DIR = "tmp"
TMP_PATH = os.path.join(os.getcwd(), TEMP_DIR)

# The maximum number of seconds worth of data a segment file can contain
# (thus can contain a max of 'MAX_SEG_LENGTH x sampling_rate' lines of data)
MAX_SEG_LENGTH = 3600


def _wfdb_dl_files(file_names):
    """
        Downloads all the requested files from the MIMIC-III Waveform Database Matched Subset
        into the temporary directory without outputting anything to console (unnecessary outputs)

        Parameters
        ----------
        file_names : list
            List of file names within the waveform database which we wish to download (each of wish
            includes a full path mapping)
    """

    sys.stdout = open(os.devnull, "w")
    wfdb.dl_files(f"mimic3wdb-matched", TMP_PATH, file_names)
    sys.stdout = sys.__stdout__


def _get_waveform_record_names(subject_prefix: str, subject_name: str, max_waveform_records: int = None):
    """
        Extracts from the RECORDS file for a subject the names of the waveform records (limited
        by 'max_waveform_records' if provided)

        Parameters
        ----------
        subject_prefix : str
            Prefix of the subject name used to organise in the waveform database (e.g. 'p01')
        subject_name : name
            The subject name in the waveform database (e.g. 'p011957')
        max_waveform_records : int
            Optional to set the maximum number of waveform record names retrieved from RECORDS

        Return
        ----------
        waveform_record_names : list
            The list of waveform record names (e.g. ['p011957-2137-03-20-10-11', 'p011957-2137-03-24-16-50', ...])
            to use to fetch names of specific segment .hea and .dat files
    """

    records_path = f"{subject_prefix}/{subject_name}/RECORDS"
    print(f"Fetching RECORDS file for subject '{subject_name}'...")
    _wfdb_dl_files([records_path])

    with open(f"{TEMP_DIR}/{records_path}") as f:
        file_lines = f.readlines()
        f.close()
    file_lines = [fl.strip() for fl in file_lines]
    waveform_record_names = [fl for fl in file_lines if fl.startswith("p") and not fl.endswith("n")]

    if max_waveform_records:
        waveform_record_names = waveform_record_names[:max_waveform_records]

    return waveform_record_names


def _get_segment_names(waveform_record_name: str, subject_prefix: str,
                       subject_name: str, max_segments_per_waveform: int = None):
    """
        Maps from the name of a waveform record of a subject name and prefix to the various
        associated segment names for that waveform record

        Parameters
        ----------
        waveform_record_name : str
            Name of the waveform record (e.g. 'p011957-2137-03-20-10-11') from which to retrieve segment names
        subject_prefix : str
            Prefix of the subject name used to organise in the waveform database (e.g. 'p01')
        subject_name : name
            The subject name in the waveform database (e.g. 'p011957')
        max_segments_per_waveform : int
            Optional to set the maximum number of segment names retrieved from the waveform record

        Return
        ----------
        segment_names : list
            The list of segment names (e.g. ['3701305_0001', '3701305_0002', ...]) to use download the
            relevant .hea and .dat files for a given subject and waveform record
    """

    # Downloads the waveform record
    wfr_path = f"{subject_prefix}/{subject_name}/{waveform_record_name}.hea"
    print(f"Fetching waveform record file '{waveform_record_name}'...")
    _wfdb_dl_files([wfr_path])

    # Extracts the segment names from the waveform record, ignoring other lines via regex
    segment_names = []
    with open(f"{TEMP_DIR}/{wfr_path}") as f:
        file_lines = f.readlines()
        for fl in file_lines:
            # Ensures also that the second part of the segment name (after underscore) is no more
            # than 4 digits, as MIMIC doesn't seem to store .hea/.dat files beyond these 4 digits
            if re.match("[0-9]+_[0-9]{1,4} [0-9]+", fl):
                # We don't include the second part of the line (the length of data in segment)
                segment_names.append(fl.split(" ")[0])
        f.close()

    if max_segments_per_waveform:
        segment_names = segment_names[:max_segments_per_waveform]

    return segment_names


def _filter_segments(segment_paths: list, both_measures: bool):
    """
        Filters out all segment paths (the segment names mapped to their location in the waveform database)
        that don't contain one or both (depending on 'both_measures') of 'PLETH' and 'ABP' signals by
        only downloading the .hea files for those segment paths (rather than downloading bigger .dat files
        for segments not containing 'PLETH'/'ABP' signals)

        Parameters
        ----------
        segment_paths : list
            The list of segment paths (e.g. ['p01/p011957/3701305_0001', 'p01/p011957/3701305_0002', ...])
            which are to be filtered
        both_measures : bool
            Set to true if need both 'PLETH' and 'ABP' in the .hea file to avoid being filtered out;
            else only need one of them

        Return
        ----------
        filtered_segment_paths : list
            The filtered 'segment_paths' containing only those who's .hea files says that the corresponding
            .dat files contain 'PLETH'/'ABP' signals
    """

    # Downloads all the header files for a given subject's waveform record
    _wfdb_dl_files([f"{seg_path}.hea" for seg_path in segment_paths])

    filtered_segment_paths = []
    for segment_path in segment_paths:
        with open(f"{TEMP_DIR}/{segment_path}.hea") as f:
            file_lines = f.readlines()
            # Loads the names of the unique signal names from the segment .hea file (skipping first line)
            signals_types = [fl.split(" ")[-1].split("\n")[0] for fl in file_lines[1:]]
            # Only want to download corresponding .dat files that have PLETH and/or ABP columns
            if both_measures:
                if "PLETH" in signals_types and "ABP" in signals_types:
                    filtered_segment_paths.append(segment_path)
            else:
                if "PLETH" in signals_types or "ABP" in signals_types:
                    filtered_segment_paths.append(segment_path)
        f.close()
    return filtered_segment_paths


def _load_segment_signals(segment_path: str):
    """
        Loads the segment 'PLETH' and/or 'ABP' signals from the provided segment path in
        the waveform database

        Parameters
        ----------
        segment_path : str
            The segment path mapping to a location in the database (e.g. 'p01/p011957/3701305_0001')

        Return
        ----------
        segment_dict : dict
            The downloaded file's data as a dictionary containing the name of the segment (e.g. '3701305_0001')
            and the 'PLETH'/'ABP' signals (empty dict if unable to load values)
    """

    print(f"Extracting segment signals from '{TMP_PATH}/{segment_path}'...")
    rd_record_path = f"{TMP_PATH}/{segment_path}".split(".hea")[0]
    try:
        record = wfdb.rdrecord(rd_record_path)
    except ValueError:
        traceback.print_exc()
        print(f"Unable to load values from segment '{rd_record_path}'...")
        return {}
    signals = record.p_signal
    sig_names = record.sig_name

    ppg = signals.T[sig_names.index("PLETH")].tolist()
    abp = signals.T[sig_names.index("ABP")].tolist()

    # Gets info from header on sample rate and number of signals, and uses this to determine the maximum
    # number of second's worth of data to be placed into a single file (i.e. MAX_SEG_LENGTH's seconds of data)
    header = wfdb.rdheader(rd_record_path)
    upload_len = header.fs * MAX_SEG_LENGTH
    num_segment_files = int(np.ceil(header.sig_len / upload_len))

    # Divides up the segment file into multiple dictionaries, each with a maximum of 'upload_len' lines,
    # and yields each one in turn
    for idx in range(num_segment_files):
        low_idx = idx * upload_len
        high_idx = min((idx + 1) * upload_len, header.sig_len)
        ppg_subset, abp_subset = ppg[low_idx:high_idx], abp[low_idx:high_idx]
        segment_dict = {"segment_name": segment_path.split("/")[-1], "file_num": idx, "sample_freq": header.fs,
                        "PPG": ppg_subset, "ABP": abp_subset}
        yield segment_dict

    # Performs cleanup when no longer needing the files (to prevent too many files existing in memory at once and
    # thus limiting chance of memory-related errors)
    os.remove(f"{TMP_PATH}/{segment_path}.hea")
    os.remove(f"{TMP_PATH}/{segment_path}.dat")


def get_subject_data_metadata(subject_name: str):
    """
        Gets the metadata about the subject data files that will be added to the metadata output files
        for a subject (i.e. waveform record name + date); utilised for when MIMIC signals
        batch processing is running with the '--only_metadata' argument

        Parameters
        ----------
        subject_name : name
            The subject name in the waveform database (e.g. 'p011957')

        Yield
        ----------
        data_metadata : dict
            The metadata about the data files themselves (and not the subject whom the data is from)
    """

    subject_prefix = subject_name[:3]
    waveform_record_names = _get_waveform_record_names(subject_prefix, subject_name)

    for waveform_record_name in waveform_record_names:
        segment_names = _get_segment_names(waveform_record_name, subject_prefix, subject_name)
        wfr_date = "-".join(waveform_record_name.split("-")[1:4])
        data_metadata = {"wfr_date": wfr_date, "wfr_name": waveform_record_name, "segment_names": segment_names}
        yield data_metadata

    # Removes the locally-downloaded files once we are done using them
    shutil.rmtree(TEMP_DIR)


def get_subject_data_segments(subject_name: str, max_waveform_records: int = None,
                              max_segments_per_waveform: int = None, both_measures: bool = True):
    """
        Yields the PPG and ABP data for the subject in question as a dictionary containing
        this data for a given segment name of a given waveform record, along with other pieces of metadata

        Parameters
        ----------
        subject_name : name
            The subject name in the waveform database (e.g. 'p011957')
        max_waveform_records : int
            Optional to set the maximum number of waveform record names retrieved from RECORDS
        max_segments_per_waveform : int
            Optional to set the maximum number of segment names retrieved from the waveform record
        both_measures : bool
            Optional set to true if need both 'PLETH' and 'ABP' in the .hea file to avoid being
            filtered out; else only need one of them

        Yield
        ----------
        data : dict
            All signals data for a subject's single segment file, with the dict containing the PPG signals,
            ABP signals, segment name, waveform record name from which the segment was sourced, and the
            waveform record date
    """

    subject_prefix = subject_name[:3]
    waveform_record_names = _get_waveform_record_names(subject_prefix, subject_name, max_waveform_records)

    for waveform_record_name in waveform_record_names:
        # Pulls the metadata for the subject in question and the date to which
        # the waveform corresponds
        segment_names = _get_segment_names(
            waveform_record_name, subject_prefix, subject_name, max_segments_per_waveform)

        # Downloads the header files for all segments for a given waveform record and filter down the list of
        # segment paths to limit the number of .dat files we need to download
        print(f"Fetching segment .hea files for waveform record '{waveform_record_name}'...")
        segment_paths = [f"{subject_prefix}/{subject_name}/{segment_name}" for segment_name in segment_names]
        try:
            segment_paths = _filter_segments(segment_paths, both_measures)
        except requests.exceptions.ConnectionError:
            print(f"ConnectionError: Connection refused by server for segment .hea files for subject "
                  f"'{subject_name}' for waveform record name '{waveform_record_name}'...")
            time.sleep(5)
            continue

        wfr_date = "-".join(waveform_record_name.split("-")[1:4])
        for segment_path in segment_paths:
            # Downloads the .dat file of the segment
            print(f"Downloading segment .dat file for waveform record '{waveform_record_name}' "
                  f"and segment path '{segment_path}'...")
            try:
                _wfdb_dl_files([f"{segment_path}.dat"])
                # Loads the segment signals from the .dat file, splitting them into hour-long chunks as necessary
                for segment_dict in _load_segment_signals(segment_path):
                    data = {"wfr_date": wfr_date, "wfr_name": waveform_record_name, **segment_dict}
                    yield data
            except requests.exceptions.ConnectionError:
                print(f"ConnectionError: Connection refused by server for segment .dat file for subject "
                      f"'{subject_name}' for waveform record name '{waveform_record_name}' and "
                      f"segment path '{segment_path}'...")
                time.sleep(5)
                continue

    # Removes the locally-downloaded files once we are done using them
    shutil.rmtree(TEMP_DIR)


def get_subject_data(subject_name: str, max_waveform_records: int = None, max_segments_per_waveform: int = None,
                     both_measures: bool = True, output_json: str = None):
    """
        Gets the PPG and ABP data for the subject in question as an embedded dictionary containing
        this data for each waveform record and each segment name, along with other pieces of metadata

        Parameters
        ----------
        subject_name : str
            The subject name in the waveform database (e.g. 'p011957')
        max_waveform_records : int
            Optional to set the maximum number of waveform record names retrieved from RECORDS
        max_segments_per_waveform : int
            Optional to set the maximum number of segment names retrieved from the waveform record
        both_measures : bool
            Optional set to true if need both 'PLETH' and 'ABP' in the .hea file to avoid being
            filtered out; else only need one of them
        output_json : str
            Optional to output the subject data to a .json file as name 'output_json'.json

        Return
        ----------
        data : list
            All signals data for a subject, with each element in the list being a dictionary for
            a given waveform record containing the dictionaries from each of the segments with
            the signal data
    """

    subject_prefix = subject_name[:3]
    waveform_record_names = _get_waveform_record_names(subject_prefix, subject_name, max_waveform_records)

    data = []
    for waveform_record_name in waveform_record_names:
        # Pulls the metadata for the subject in question and the date to which
        # the waveform corresponds
        segment_names = _get_segment_names(
            waveform_record_name, subject_prefix, subject_name, max_segments_per_waveform)

        # Downloads the header files for all segments for a given waveform record and filter down the list of
        # segment paths to limit the number of .dat files we need to download
        print(f"Fetching segment .hea files for waveform record '{waveform_record_name}'...")
        segment_paths = [f"{subject_prefix}/{subject_name}/{segment_name}" for segment_name in segment_names]
        segment_paths = _filter_segments(segment_paths, both_measures)

        # Downloads the .dat files of the segments
        print(f"Fetching segment .dat files for waveform record '{waveform_record_name}'...")
        _wfdb_dl_files([f"{seg_path}.dat" for seg_path in segment_paths])

        segment_signals = []
        for segment_path in segment_paths:

            # As '_load_segment_signals()' yields a single dictionary, gets all the yielded dictionaries and
            # combines their PPG and ABP signal data together
            segment_dicts = [s_d for s_d in _load_segment_signals(segment_path)]
            segment_dict = {"segment_name": segment_dicts[0]["segment_name"],
                            "sample_freq": segment_dicts[0]["sample_freq"],
                            "PPG": [val for s_d in segment_dicts for val in s_d["PPG"]],
                            "ABP": [val for s_d in segment_dicts for val in s_d["ABP"]]}

            # Only appends the signals if they contain both PPG and ABP signals
            if segment_dict:
                segment_signals.append(segment_dict)
        wfr_date = "-".join(waveform_record_name.split("-")[1:4])
        inner_data = {"wfr_date": wfr_date, "wfr_name": waveform_record_name, "segments": segment_signals}
        data.append(inner_data)

    # Removes the locally-downloaded files once we are done using them
    shutil.rmtree(TEMP_DIR)

    if output_json:
        with open(f"{output_json}.json", "w") as f:
            json.dump(data, f)

    return data


if __name__ == "__main__":
    # d = get_subject_data(subject_name="p072992", max_waveform_records=2,
    #                      max_segments_per_waveform=50, both_measures=True)
    # print(d)
    for seg in get_subject_data_metadata("p000107"):
        print(seg)
