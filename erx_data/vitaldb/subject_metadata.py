import numpy as np
import pandas as pd


def get_subject_metadata(subject_id: int):
    """
        Gets subject metadata for the subject in question as a dictionary of all fields and their
        values from the Clinical Information List from VitalDb

        Parameters
        ----------
        subject_id : int
            The subject ID in the VitalDB database (e.g. 2348)

        Return
        ----------
        data : dict
            All metadata available for the subject, with keys being the metadata fields and
            values being the metadata values
    """

    df_cases = pd.read_csv("https://api.vitaldb.net/cases")
    subject_meta = df_cases.loc[(df_cases["caseid"] == subject_id)].iloc[0, :].to_dict()

    # Converts the types of fields to non-numpy values (to ensure ints and floats get written as intended
    # to .json files) and changed types for some specific fields that make more sense (e.g. int for 'Age')
    for k, v in subject_meta.items():
        # The 'Y/N'-fields are stored as 1's or 0's, so more intuitive to interpret them as bools
        if k in ("emop", "death_inhosp", "preop_htn", "preop_dm"):
            subject_meta[k] = True if v == 1 else False
        # Age being stored as float for some reason in VitalDB, so want to force it as int
        elif k == "age" or isinstance(v, np.int64):
            subject_meta[k] = int(v)
        elif isinstance(v, np.float64):
            if np.isnan(v):
                subject_meta[k] = None
            else:
                subject_meta[k] = float(v)
        elif isinstance(v, float) and np.isnan(v):
            subject_meta[k] = None

    return subject_meta


if __name__ == "__main__":
    s_id = 2348
    s_meta = get_subject_metadata(s_id)
    for k, v in s_meta.items():
        print(f"{k} : {v}")
