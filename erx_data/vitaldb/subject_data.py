import numpy as np
import pandas as pd


# The maximum number of seconds worth of data a segment file can contain
# (thus can contain a max of 'MAX_SEG_LENGTH x sampling_rate' lines of data)
MAX_SEG_LENGTH = 3600

# Set to this value as standard for all VitalDB subjects
# (see https://vitaldb.net/data-bank/?query=api&documentId=1_VTteoI5v-cdFkUTSnhYoGCqyq8dAC-
# _k50oM1QOjkU&sectionId=h.9843xaaeez5w)
SAMPLE_FREQ = 500


def get_subject_data_segments(subject_id: int):
    """
        Yields the PPG and ABP data for the subject in question as a single chunk of the total file data,
        along with other pieces of metadata

        Parameters
        ----------
        subject_id : int
            The subject ID in the VitalDB database (e.g. 2348)

        Yield
        ----------
        data : dict
            A single file's worth of data for a subject (the size determined by the the maximum segment length in
            seconds and the sampling frequency), with the dict containing the PPG signals, the ABP signals,
            the sampling frequency, and the file chunk index
    """

    # Loads the ID strings for the ART and PLETH measurements from the 'trks' table
    df_trks = pd.read_csv("https://api.vitaldb.net/trks")
    try:
        subject_art_id = df_trks.loc[(df_trks["caseid"] == subject_id) &
                                     (df_trks["tname"] == "SNUADC/ART")].iloc[0, 2]
        subject_pleth_id = df_trks.loc[(df_trks["caseid"] == subject_id) &
                                       (df_trks["tname"] == "SNUADC/PLETH")].iloc[0, 2]
    except IndexError:
        print(f"Unable to find PLETH/ART signal(s) for subject '{subject_id}'...")
        return None

    # Uses the ID strings to load and join together the signals for ART and PLETH
    df = pd.merge(pd.read_csv(f"https://api.vitaldb.net/{subject_art_id}").drop(columns=['Time']),
                  pd.read_csv(f"https://api.vitaldb.net/{subject_pleth_id}").drop(columns=['Time']),
                  left_index=True, right_index=True)
    # Renames the columns to names we're expecting and swap their order (to be consistent w/ MIMIC subject data)
    df.columns = ["ABP", "PPG"]
    df = df[["PPG", "ABP"]]

    upload_len = SAMPLE_FREQ * MAX_SEG_LENGTH
    num_segment_files = int(np.ceil(len(df.index) / upload_len))

    for idx in range(num_segment_files):
        low_idx = idx * upload_len
        high_idx = min((idx + 1) * upload_len, len(df.index))
        df_subset = df.iloc[low_idx:high_idx, :]
        data = {**df_subset.to_dict("list"), "file_num": idx, "sample_freq": SAMPLE_FREQ}
        yield data


def get_subject_data(subject_id: int):
    """
        Gets the PPG and ABP data for the subject in question as a dictionary containing
        the signal data for the subject

        Parameters
        ----------
        subject_id : int
            The subject ID in the VitalDB database (e.g. 2348)

        Return
        ----------
        data : dict
            All signals data for a subject, with each key being a signal name and the value being
            a list of the signal values
    """

    # Loads the ID strings for the ART and PLETH measurements from the 'trks' table
    df_trks = pd.read_csv("https://api.vitaldb.net/trks")
    try:
        subject_art_id = df_trks.loc[(df_trks["caseid"] == subject_id) &
                                     (df_trks["tname"] == "SNUADC/ART")].iloc[0, 2]
        subject_pleth_id = df_trks.loc[(df_trks["caseid"] == subject_id) &
                                       (df_trks["tname"] == "SNUADC/PLETH")].iloc[0, 2]
    except IndexError:
        print(f"Unable to find PLETH/ART signal(s) for subject '{subject_id}'...")
        return None

    # Uses the ID strings to load and join together the signals for ART and PLETH
    df = pd.merge(pd.read_csv(f"https://api.vitaldb.net/{subject_art_id}").drop(columns=['Time']),
                  pd.read_csv(f"https://api.vitaldb.net/{subject_pleth_id}").drop(columns=['Time']),
                  left_index=True, right_index=True)
    # Renames the columns to names we're expecting and swap their order (to be consistent w/ MIMIC subject data)
    df.columns = ["ABP", "PPG"]
    df = df[["PPG", "ABP"]]

    data = {**df.to_dict("list"), "sample_freq": SAMPLE_FREQ}

    return data


if __name__ == "__main__":
    s_id = 2348
    d = get_subject_data(2348)
    for k, v in d.items():
        print(f"{k} : {v}")
